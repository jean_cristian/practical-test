package com.example.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * The Class SwaggerConfig.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	/**
	 * Api.
	 *
	 * @return the docket
	 */
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.example.api.web.rest")).paths(PathSelectors.any())
				.build().pathMapping("/").apiInfo(this.apiInfo());
	}

	/**
	 * Api info.
	 *
	 * @return the api info
	 */
	private ApiInfo apiInfo() {

		return new ApiInfoBuilder().title("Template SB_RST_SW_SPR_DTA")
				.description(
						"This is a template for : Spring Boot, Rest, Swagger with Spring Data and H2-database.")
				.version("1.0.0").contact(getContact()).build();

	}

	/**
	 * Gets the contact.
	 *
	 * @return the contact
	 */
	private Contact getContact() {
		return new Contact("EXAMPLE SOLUCOES S/A", "https://www.example.com", "");
	}

}
