package com.example.api.service;

import com.example.api.domain.Customer;
import com.example.api.exeption.ServiceException;
import com.example.api.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {

    private CustomerRepository repository;

    @Autowired
    public CustomerService(CustomerRepository repository) {
        this.repository = repository;
    }

    public List<Customer> findAll() {
        return repository.findAllByOrderByNameAsc();
    }

    public Optional<Customer> findById(Long id) {
        return repository.findById(id);
    }

    public Customer save(Customer customer) throws ServiceException {
        return repository.save(customer);
    }

    public void saveAll(List<Customer> customers) throws ServiceException {
        repository.saveAll(customers);
    }

    public void delete(final Customer customer) throws ServiceException {
        repository.delete(customer);
    }

    public void update(final Customer customer, final Long id) {
        customer.setId(id);
        repository.save(customer);
    }

    public List<Customer> search(int page, int size) {
        PageRequest pageRequest = PageRequest.of(
                page,
                size,
                Sort.Direction.ASC,
                "id");

        return repository.findAll(pageRequest).getContent();
    }

}
