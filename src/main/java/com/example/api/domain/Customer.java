package com.example.api.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @NotNull(message = "É necessário ao menos um nome de cliente!")
    @NotEmpty(message = "É necessário ao menos um nome de cliente!")
    private String name;

    @Column(nullable = false)
    @Email
    @NotNull(message = "É necessário ao menos um email de cliente!")
    @NotEmpty(message = "É necessário ao menos um email de cliente!")
    private String email;

}
