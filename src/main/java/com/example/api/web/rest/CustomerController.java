package com.example.api.web.rest;

import com.example.api.domain.Customer;
import com.example.api.exeption.HelperException;
import com.example.api.exeption.RestException;
import com.example.api.exeption.ServiceException;
import com.example.api.helper.CustomerHelper;
import com.example.api.service.CustomerService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    private final static Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

    private CustomerService service;

    @Autowired
    public CustomerController(CustomerService service) {
        this.service = service;
    }

    @Autowired
    CustomerHelper customerHelper;

    @ApiOperation(value = "list", notes = "This operation list all entities", response = String.class)
    @GetMapping
    public List<Customer> findAll() {
        return service.findAll();
    }

    @ApiOperation(value = "list", notes = "This operation listing all entities by Page and size", response = Page.class)
    @GetMapping("/list")
    public ResponseEntity listAll(@RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                  @RequestParam(value = "size", required = false, defaultValue = "10") int size) throws RestException {
        return ResponseEntity.ok(service.search(page, size));
    }

    @ApiOperation(value = "get", notes = "This operation gets an entity", response = String.class)
    @GetMapping("/{id}")
    public Customer findById(@PathVariable Long id) {
        return service.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Customer not found"));
    }

    @ApiOperation(value = "create", notes = "This operation creates a new entity", response = String.class)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> create(@Valid @RequestBody final Customer request) throws RestException {
        try {
            service.save(request);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
            throw new RestException(e.getMessage(), e);
        }

    }

    @ApiOperation(value = "create", notes = "This operation creates a new entities", response = String.class)
    @PostMapping(value = "/createAll", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createAll(@Valid @RequestBody final List<Customer> request) throws RestException {
        try {
            service.saveAll(request);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
            throw new RestException(e.getMessage(), e);
        }

    }

    @ApiOperation(value = "update", notes = "This operation updates an entity", response = String.class)
    @PutMapping(value = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<String> update(@Valid @RequestBody final Customer request, @PathVariable final Long id) throws HelperException, RestException {
        try {
            service.update(customerHelper.factoryCustomer(request), id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
            throw new RestException(e.getMessage(), e);
        }

    }

    @ApiOperation(value = "delete", notes = "This operation deletes an entity", response = String.class)
    @DeleteMapping(value = "{id}")
    public @ResponseBody
    ResponseEntity<String> delete(@PathVariable final Long id) throws RestException {
        try {
            service.delete(service.findById(id).get());
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
            throw new RestException(e.getMessage(), e);
        }

    }

}
