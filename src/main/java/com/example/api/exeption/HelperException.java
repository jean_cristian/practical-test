package com.example.api.exeption;

/**
 * The Class HelperException.
 */
public class HelperException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5723481590898846852L;

	/**
	 * Instantiates a new helper exception.
	 */
	public HelperException() {

		super();
	}

	/**
	 * Instantiates a new helper exception.
	 *
	 * @param message the message
	 */
	public HelperException(final String message) {

		super(message);
	}

	/**
	 * Instantiates a new helper exception.
	 *
	 * @param exception the exception
	 */
	public HelperException(final Exception exception) {

		super(exception);
	}

	/**
	 * Instantiates a new helper exception.
	 *
	 * @param throwable the throwable
	 */
	public HelperException(final Throwable throwable) {

		super(throwable);
	}

	/**
	 * Instantiates a new helper exception.
	 *
	 * @param message   the message
	 * @param exception the exception
	 */
	public HelperException(final String message, final Exception exception) {

		super(message, exception);
	}

	/**
	 * Instantiates a new helper exception.
	 *
	 * @param message   the message
	 * @param throwable the throwable
	 */
	public HelperException(final String message, final Throwable throwable) {

		super(message, throwable);
	}

}
