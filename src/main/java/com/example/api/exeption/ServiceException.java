package com.example.api.exeption;

/**
 * The Class ServiceException.
 */
public class ServiceException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7952525688481224306L;

	/**
	 * Instantiates a new service exception.
	 */
	public ServiceException() {

		super();
	}

	/**
	 * Instantiates a new service exception.
	 *
	 * @param message the message
	 */
	public ServiceException(final String message) {

		super(message);
	}

	/**
	 * Instantiates a new service exception.
	 *
	 * @param exception the exception
	 */
	public ServiceException(final Exception exception) {

		super(exception);
	}

	/**
	 * Instantiates a new service exception.
	 *
	 * @param throwable the throwable
	 */
	public ServiceException(final Throwable throwable) {

		super(throwable);
	}

	/**
	 * Instantiates a new service exception.
	 *
	 * @param message   the message
	 * @param exception the exception
	 */
	public ServiceException(final String message, final Exception exception) {

		super(message, exception);
	}

	/**
	 * Instantiates a new service exception.
	 *
	 * @param message   the message
	 * @param throwable the throwable
	 */
	public ServiceException(final String message, final Throwable throwable) {

		super(message, throwable);
	}
}
