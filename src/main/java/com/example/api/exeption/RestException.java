package com.example.api.exeption;

/**
 * The Class RestException.
 */
public class RestException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4409282395721669310L;

	/**
	 * Instantiates a new rest exception.
	 */
	public RestException() {

		super();
	}

	/**
	 * Instantiates a new rest exception.
	 *
	 * @param message the message
	 */
	public RestException(final String message) {

		super(message);
	}

	/**
	 * Instantiates a new rest exception.
	 *
	 * @param exception the exception
	 */
	public RestException(final Exception exception) {

		super(exception);
	}

	/**
	 * Instantiates a new rest exception.
	 *
	 * @param throwable the throwable
	 */
	public RestException(final Throwable throwable) {

		super(throwable);
	}

	/**
	 * Instantiates a new rest exception.
	 *
	 * @param message   the message
	 * @param exception the exception
	 */
	public RestException(final String message, final Exception exception) {

		super(message, exception);
	}

	/**
	 * Instantiates a new rest exception.
	 *
	 * @param message   the message
	 * @param throwable the throwable
	 */
	public RestException(final String message, final Throwable throwable) {

		super(message, throwable);
	}

}
