package com.example.api.helper;

import com.example.api.domain.Customer;
import com.example.api.exeption.ServiceException;
import org.springframework.stereotype.Component;

@Component
public class CustomerHelper {

    public Customer factoryCustomer(Customer request) throws ServiceException {

        try {
            final Customer customer = new Customer();
            customer.setEmail(request.getEmail());
            customer.setName(request.getName());
            return customer;
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }
}