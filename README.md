# Custumer Service

### Requisitos

1. JDK 8
1. Maven 3

### Rodando

1. Clone o projeto: `https://github.com/allanalves92/practical-test.git`
2. Entre na pasta `practical-test` e execute: `mvn spring-boot:run`
3. Acesse: `http://localhost:8080/customers`
4. Acessar via Swagger , após rodar a aplicação, acessar a URL :  http://localhost:8080/swagger-ui.html

# Tecnologias

**Lombok** - para produtividade, ele fica responsável por gerar os Getter e Setter; Builder; Construtor, entre outros.

**Springboot** - Com configurações rápidas, você consegue, disponibilizar uma aplicação baseada no Spring. No caso disponibilizamos uma API.

**swagger** - é usado , construir, documentar e usar serviços da Web RESTful

**H2** - H2 é um banco de dados relacional gerado em memoria durante a execução da aplicação
